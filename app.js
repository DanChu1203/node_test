const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(cookieParser());

//Renders index with header information
app.get('/', function (req, res) {
	console.log("index");

	res.cookie('cookie_name' , 'cookie_value');
	res.cookie('cookie_name2' , 'cookie_value2');
	res.cookie('cookie_name3' , 'cookie_value3');

  // Cookies that have been signed
  console.log('Signed Cookies: ', req.signedCookies)

  res.render('index.ejs', { headers: req.headers, keys: Object.keys(req.headers) });

})

//Post route show page to display form data
app.post('/', function (req, res) {
  console.log('POST request');

  console.log('Cookies: ', req.cookies)

  // Cookies that have been signed
  console.log('Signed Cookies: ', req.signedCookies)

  res.render('show.ejs', { body_hashes: req.body, keys: Object.keys(req.body), headers: req.headers, header_keys: Object.keys(req.headers), cookies: req.cookies, cookie_keys: Object.keys(req.cookies), title: req.body["title"] });

  res.end();
})

//Listens for requests on port 3000 or specified port
if (process.argv.length >= 3) {

  app.listen(process.argv[2], function () {
    console.log('Example app listening on port ' + process.argv[2] + '!');
  });
} else {
  app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
  });
}